package com.example.devapp.adverbsec;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

public class MainActivity extends AppCompatActivity implements RewardedVideoAdListener {

     AdView mAdView;
     AdView mAdView1;

    private Button videoLoadBtn;

    private int mCoinCount;
    private TextView mCoinCountText;

    public static final String COIN_COUNT_KEY = "COIN_COUNT";

    private RewardedVideoAd mAd;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Use an activity context to get the rewarded video instance.
        MobileAds.initialize(this, "ca-app-pub-6380552903367935~9743959161");

        videoLoadBtn = (Button)findViewById(R.id.watch_video_btn);
        mCoinCountText = (TextView)findViewById(R.id.number_points_txt);


        mAd = MobileAds.getRewardedVideoAdInstance(this);
        mAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();


        // initialize and load banners on top and bottom
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView1 = (AdView) findViewById(R.id.adView1);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        mAdView1.loadAd(adRequest1);

        // When press on button to view video
        videoLoadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mAd.isLoaded()) {
                    mAd.show();
                }

            }
        });
        
        // если что, то добавляем поинт

        if (savedInstanceState == null) {
            mCoinCount = 0;
            mCoinCountText.setText("Coins: " + mCoinCount);

            loadRewardedVideoAd();
        }


    }

    private void loadRewardedVideoAd() {
        mAd.loadAd("ca-app-pub-6380552903367935/4783631740", new AdRequest.Builder().build());
    }

    @Override
    public void onRewardedVideoAdLoaded() {

    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {
        loadRewardedVideoAd();
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {

        addCoins(rewardItem.getAmount());

    }

    private void addCoins(int amount) {
        mCoinCount = mCoinCount + amount;
        mCoinCountText.setText("Coins: " + mCoinCount);
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Toast.makeText(getBaseContext(), "Ad failed to load.", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onPause() {
        mAd.pause(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        mAd.resume(this);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mAd.destroy(this);
        super.onDestroy();
    }
}
